

object Calculator {
  def main(args: Array[String]) {
    
    def readNumbers(): (Double, Double) = {
      println("Write the first number")
      val first = Console.readDouble()
      println("Write the seconds number")
      val second = Console.readDouble()
      (first, second)
    }
    
    val (a, b) = readNumbers()
    
    println("The sum of the two numbers are " + (a + b))
    println("The difference of the two numbers are " + (a - b))
    println("The product of the two numbers are " + (a * b))
    println("The quotient of the two numbers are " + (a / b))
  }
}